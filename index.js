const Epxress = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const connectionString = "mongodb+srv://warhammerdev:Pw4warhammer123@cluster0-oj7cw.mongodb.net/test?retryWrites=true&w=majority";
mongoose.connect(connectionString, { useNewUrlParser: true });
const app = Epxress();
const bcrypt = require('bcrypt');
console.clear();
const Schema = mongoose.Schema;


const userSchema = new Schema({
  password: String,
  username: String,
  login: String,
  characters: [{ type: Schema.Types.ObjectId, ref: 'Character'}],
});

const characterSchema = new Schema({
  nickname: String,
  level: Number,
  kills: Number,
  deaths: Number,
  xp: Number,
  type: String,
  user: { type: Schema.Types.ObjectId, ref: 'User'},
})

const User = mongoose.model('User', userSchema);
const Character = mongoose.model('Character', characterSchema);

createTestChar();

function createTestChar(){
  User.findOne({ login: 'dev' }).exec()
  .then((user) =>{
    new Character({nickname: 'Testchar 1', level: 1, kills: 100, deaths: 20, xp: 5000, type: "Warrior", user: user}).save();
  })
  .catch((err) => {
    console.error(err);
  })

}


async function validateLogin(login, password) {
  return new Promise((resolve, reject) => {
    User.findOne({ login: login }).exec().then((user) => {
      resolve(bcrypt.compareSync(password, user.password))
    })
      .catch((err) => {
        reject(err);
      })
  })
}

app.use(bodyParser.json());
app.get("/test", async (req, res) => {
  let result = await validateLogin(req.query.login, req.query.password);
});

app.get("/auth", async (req, res) => {
  if (!req.query.login && !req.query.password) {
    let correct = await validateLogin(req.query.login, req.query.password);
    let response = {};
    switch (correct) {
      case true:
        response = { ResultCode: 1, Message: "Connected" };
        break;
      case false:
        response = { ResultCode: 2, Message: "Wrong data" };
        break;
    }
    res.json(response);
  } else {
    res.json({ ResultCode: 3, Message: "Missing fields" });
  }

})

app.post("/register", (req, res) => {
  console.log(req.body);
  res.json({ responseCode: "Works" });
})

app.listen(3001, () => {
  console.log("Server running on port 3001");
})
